package com.shop.action;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import com.opensymphony.xwork2.ActionSupport;
import com.shop.dao.Cart;

public class ShowCartAction extends ActionSupport implements SessionAware {
	
	private Map sessionsMap;
	private Cart cart;

	public Cart getCart() {
		return cart;
	}

	public String showCart() {

		cart = (Cart) sessionsMap.get("Cart");
		if (cart == null) {
			cart = new Cart();
		}
		return SUCCESS;
	}

	@Override
	public void setSession(Map arg0) {

		sessionsMap = arg0;

	}

}
