package com.shop.action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.shop.dao.Cart;
import com.shop.dao.Catalog;
import com.shop.model.Product;

public class CatalogAction extends ActionSupport implements SessionAware{
	
	private List<Product> products;
	private int cartItemsCount;
	private Map sessionMap;
	
	public String catalog(){
		products = Catalog.getProducts();
		return SUCCESS;
	}
	
	public List<Product> getProducts() {
		return products;
	}
	
	public int getCartItemsCount() {
		Cart cart = (Cart) sessionMap.get("Cart");
		return cart == null?0:cart.getProducts().size();
	}

	@Override
	public void setSession(Map arg0) {
		sessionMap = arg0;
		
	}
}
