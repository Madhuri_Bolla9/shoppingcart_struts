package com.shop.action;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.shop.dao.Cart;
import com.shop.dao.Catalog;
import com.shop.model.Product;

public class AddToCartAction extends ActionSupport implements SessionAware {
	private int id;
	private Map sessionMap;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String addToCart() {
		
		Product product = Catalog.getProductById(id);
		if (product != null) {

			Cart cart = (Cart) sessionMap.get("Cart");
			if (cart == null) {
				cart = new Cart();
				sessionMap.put("Cart", cart);
			}
			cart.addToCart(product);
		}
		

		return SUCCESS;
	}

	@Override
	public void setSession(Map arg0) {
		sessionMap = arg0;
	}

}
