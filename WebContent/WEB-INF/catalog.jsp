<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Catalog</title>
</head>
<body>
	<H1>Product Catalog</H1>
	Items in Cart: <s:property value="cartItemsCount"/> <a href="/ShoppingCartStruts/showCart.action">Show Cart</a>
	<table width=100%>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Qty</th>
			<th>Price</th>
			<th></th>
		</tr>
		
		<s:iterator value = "products">
			<tr>
				<td><s:property value="id"/></td>
				<td><s:property value="name"/></td>
				<td><s:property value="qty"/></td>
				<td><s:property value="price"/></td>
				<td>	
					<s:if test="qty>0">
						<a href="/ShoppingCartStruts/addToCart.action?id=<s:property value="id"/>">Add to cart</a>
					</s:if>
				</td>
			</tr>
		</s:iterator>
	</table>
</body>
</html>