<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cart</title>
</head>
<body>
	<a href="/ShoppingCartStruts/catalog.action">Back to catalog</a>
	<table width=50%>
			<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Qty</th>
			<th>Item totalPrice</th>
			<th></th>
		</tr>
		
		<s:iterator value= "cart.products">
			<tr>
				<td><s:property value="product.id"/></td>
				<td><s:property value="product.name"/></td>
				<td><s:property value="qty"/></td>
				<td><s:property value="price"/></td>
				<td><a href="/ShoppingCartStruts/removeFromCart.action?id=<s:property value="product.id"/>">Remove from cart</a></td>
			</tr>
		</s:iterator>
		<tr>
			<td>Total Price: <s:property value="cart.price"/></td>
		</tr>
		
	</table>
</body>
</html>